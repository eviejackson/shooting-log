import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * A class to handle and validate user input where a menu index is not sufficient, before passing back to where the
 * method was invoked in order to minimise potential errors.
 *
 * @author E.Jackson
 *
 */
public class IO {

    static Scanner sc = new Scanner(System.in);

    /**
     * Method to validate values
     *
     * If the value is a valid float number and not 0, it is accepted and
     * the value is passed back to where it was invoked from. If the given value is not valid, the user is asked
     * to enter the value again until they give a valid value.
     *
     * Strips ',' and ' ' from inputted values such that formats '1000', '1,000', and '1 000' are accepted.
     *
     * @param prompt Message to be outputted to the user
     * @return Validated value
     */
    //Integer-Validates and strips chr(32)[space] and chr(44)[,] from input string
    //Keep as float, decimal point scores possible on scores higher than 100
    public static float ioValue (String prompt) {

        float output = 0;

        boolean valid = false;
        while (!valid) {
            System.out.println(prompt);
            String input = sc.nextLine();

            input = input.replace(" ", "");
            input = input.replace(",", "");

            try {
                output = Float.parseFloat(input);
                if (output != 0 && output < 101.1) {
                    valid = true;
                } else {
                    System.out.println("Error! Score must have a value greater than 0.");
                }
            } catch(Exception ignored) {}

        }

        return output;

    }

    /**
     * Method to validate month
     *
     * If the month entered is a valid string or integer representation of the month number, it is accepted and
     * the month number is passed back to where it was invoked from. If the given month is not valid, the user is asked
     * to enter the month again until they give a valid postcode.
     *
     * When input is a string, the first index is capitalised, and the rest are made lower case. When the inputted
     * string is 3 characters long, it is checked with a DateTimeFormatter (format: short), and the boolean flag is made
     * true to indicate a valid month has been entered. If the inputted string is not 3 characters, it is checked with a
     * DateTimeFormatter (format: long), and the boolean flag is made true to indicate a valid month has been entered.
     * At this point, the month is then converted back to the month number and passed back to the place where the method
     * was invoked. If this hasn't worked, the input is converted to an integer and the value is checked. If it's less
     * than 13 it is accepted. Otherwise, the user is asked again to enter a valid month in one of three forms.
     *
     * @param prompt Message to be outputted to the user
     * @return Validated month in correct format
     */
    //Integer-from String Input. Takes 3 representations of Month and outputs as month number.
    public static Integer ioMonth (String prompt) {

        String input;
        int output = 0;
        //Make sure the month is valid, if it isn't, loop

        boolean valid = false;
        while (!valid) {
            System.out.println(prompt);
            input = sc.nextLine();

            /*
            Code to convert text to number sourced here: https://stackoverflow.com/questions/2268969/convert-month-string-to-integer-in-java
             */

            /*
            Exceptions ignored in some try statements below, this is because only 1 of 3 methods are expected to work.
            If no methods work, the user is asked to reenter the date.
            */

            //Ensure input string is sentence case-DateTimeFormatter is case sensitive
            input = input.substring(0,1).toUpperCase() + input.substring(1).toLowerCase();

            if (input.length() == 3) {
                try {
                    //Converts expected format (short: eg, feb) to Calendar Number
                    DateTimeFormatter parser = DateTimeFormatter.ofPattern("MMM").withLocale(Locale.ENGLISH);
                    TemporalAccessor accessor = parser.parse(input);
                    output = accessor.get(ChronoField.MONTH_OF_YEAR);
                    valid = true;
                } catch(Exception ignored) {}
            } else {
                try {
                    //Converts expected format (short: eg, feb) to Calendar Number
                    DateTimeFormatter parser = DateTimeFormatter.ofPattern("MMMM").withLocale(Locale.ENGLISH);
                    TemporalAccessor accessor = parser.parse(input);
                    output = accessor.get(ChronoField.MONTH_OF_YEAR);
                    valid = true;
                } catch(Exception ignored) {}
                try {
                    output = Integer.parseInt(input);
                    if (output < 13) {
                        valid = true;
                        break;
                    }
                } catch(Exception ignored) {}
            }
        }

        return output;

    }

    /**
     * Method to validate year
     *
     * If the year is a valid integer, it is accepted and the value is passed back to where it was invoked from. If the
     * given year is not valid (eg. in the future), the user is asked to enter the year again until it is valid.
     *
     * @param prompt Message to be outputted to the user
     * @return Validated year
     */
    //Integer-from String Input
    public static Integer ioYear (String prompt) {

        Integer input = 0;

        //Make sure the year is valid, if it isn't, loop
        boolean valid = false;
        while (!valid) {
            System.out.println(prompt);

            try {
                input = sc.nextInt();
                int currentYear = LocalDate.now().getYear();

                if (input <= currentYear) {
                    valid = true;
                } else {
                    System.out.println("Error! That is in the future! [Sadly, we can't yet time travel :( ]");
                }
            } catch(Exception ignored) {}

        }

        return input;

    }


}


import java.util.*;

/**
 *
 * A class to run and manage the execution of Shooting Log
 *
 * @author E.Jackson
 *
 */
public class ShootingLog {

    /**
     * main method calls run_menu() method which manages execution of the Shooting Log System
     *
     * @param args
     */
    public static void main(String[] args) {

        System.out.println("Welcome to Shooting Log");

        //In a system where data is drawn from files or external databases, load and initialise everything here

        new ShootingLog().run();

    }

    /**
     * Method to continually run Shooting Log until user decides to exit
     *
     * The user is asked to select a function from the main menu, and hence the following methods called enable the
     * specified action to be executed. On completion, the user is returned to the main menu until they decide to exit.
     */
    private void run() {

        boolean continue_flag = true;
        int choice;

        //Run program and return to main menu until user exits
        while (continue_flag) {

            choice = Menu.menuIO(Arrays.asList("Log",
                    "Overview",
                    "Exit"));
                    /*
                    Options to implement

                    "Find a Session",
                    "Match Cards",
                    "Practice Cards",
                    "Graphical Analysis"
                    */

            //Log a session
            //Overview
            //Confirm user wants to exit
            //No valid option selected
            switch (choice) {
                case 1 -> {
                    String outputFromLogSession = logSession();
                    if (outputFromLogSession.equals("false")) {
                        System.out.println("Session not logged.");
                    } else {
                        System.out.println(outputFromLogSession);
                    }
                }
                case 2 -> System.out.println("Overview output should be printed");
                case 3 -> {
                    System.out.println("Are you sure?");
                    List<String> confirm = new ArrayList<>(Arrays.asList("No", "Yes"));
                    int confirm_choice = Menu.menuIO(confirm);
                    if (confirm_choice == 2) {
                        continue_flag = false;
                        System.out.println("Closing Shooting Log...");
                    }
                }
                default -> System.out.println("Not a valid option, please try again");
            }
        }

    }

    private static String logSession(){

        //Tells invoker whether the method saved a session or not
        String success = "false";

        Session sessionAdded = new Session("Match", 10, 10, 100, 97);
        success = sessionAdded.toString();

        //TODO: Assign session.toString to success if it was saved successfully
        return success;

    }

}

import java.util.List;
import java.util.Scanner;

public class Menu {

    /**
     * Method to generate menu output & indexes from a given list of possible options, and prevent selection of out of
     * range indexes.
     *
     * Takes a list of valid options, and assigns an integer to each option based on the position in the list. It then
     * takes and validated the user's specified option, which is then returned to the place where menuIO was invoked from.
     *
     * @param Options List of possible options a user should be able to choose
     * @return Validated index of selected option
     */
    public static int menuIO(List<String> Options) {

        //Method to take in a list of options and generate the correct output with indexes
        //Validates input to make sure it is an Integer

        //Generate Options to be printed with correct index

        System.out.println("Please select an option...");
        for (String choice : Options) {
            System.out.println((Options.indexOf(choice) + 1) + " - " + choice);
        }

        //Get user input
        Scanner sc = new Scanner(System.in);
        int choice;

        //Check input token is an integer
        do {
            while (!sc.hasNextInt()) {
                System.out.println("That is not a number! \nPlease enter a menu option");
                sc.next();
            }
            choice = sc.nextInt();
        } while (choice < 0);

        //Pass choice back to menu
        return choice;

    }

}
